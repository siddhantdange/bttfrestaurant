
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("checkAccount", function(request, response) {
     var query = new Parse.Query("Restaurant");
     query.equalTo("email", request.params.email);
     query.find({success : function(results){
	  //if the account exists, check to see if password matches
          //if no account exists, make a new account
          if(results.length){
	       if(results[0].get("password") == request.params.password){
                    response.success({account: results[0], status: 1});
               } else{
                    response.success({account: undefined, status: -2});
               }
	  } else{
	       response.success({account: undefined, status: -1});
          }
     }, error: function(){
          response.success({account: undefined, status: -1000});
     }}); 
});

Parse.Cloud.define("chargeRestaurant", function(request, response){
	var Stripe = require('stripe');
	Stripe.initialize('sk_test_xy8hiDj0Jjrc3d7bw7BF9L8c');
	var query = new Parse.Query("Restaurant");
	query.equalTo("email", request.params.email);
	query.find({success: function(results){
		if(results.length){
			var token = results[0].get("stripeToken");
			if(token == undefined){
				token = request.params.stripeToken;
			}
			
			Stripe.Charges.create({
                                        amount : request.params.totalCost,
                                        currency : "usd",
                                        card : request.params.stripeToken
                        }, {
			success: function(httpResponse){
				response.success({status : 10000});
			},
			error: function(httpResponse){
				response.success({status: -10000, response: httpResponse, responseMessage: httpResponse['message']});
			}
			});
		}

		}, error: function(){
			response.success({status : -5000});
		}
	});
});

