//
//  PFObject+Extension.h
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 11/27/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import <Parse/Parse.h>

@interface PFObject (Extension) <NSCoding>

@end
