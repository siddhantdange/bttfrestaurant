//
//  OrdersViewController.h
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 12/3/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrdersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
