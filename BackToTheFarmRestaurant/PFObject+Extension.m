//
//  PFObject+Extension.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 11/27/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "PFObject+Extension.h"
#import "Constants.h"

@implementation PFObject (Extension)

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    NSArray *attributesArr = nil;
    NSString *classKey = [aDecoder decodeObjectForKey:@"classKey"];
    self = [[PFObject alloc] initWithClassName:classKey];
    
    if([classKey isEqualToString:CACHE_KEY_ACCOUNT]){
        attributesArr = OBJECT_ACCOUNT_ATTRIBUTES;
    } else if([classKey isEqualToString:CACHE_KEY_FOODS]){
        attributesArr = OBJECT_FOODS_ATTRIBUTES;
    }
    
    for(NSString *key in attributesArr){
        NSObject *decodedObj = [aDecoder decodeObjectForKey:key];
        if(!decodedObj){
            decodedObj = [NSNull null];
        }
        
        [self setObject:decodedObj forKey:key];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    for(NSString *key in self.allKeys){
        [aCoder encodeObject:self[key] forKey:key];
    }
    
    [aCoder encodeObject:self.parseClassName forKey:@"classKey"];
}

@end
