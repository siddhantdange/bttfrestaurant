//
//  FoodCell.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 12/2/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "FoodCell.h"

@implementation FoodCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
