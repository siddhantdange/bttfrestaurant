//
//  UIAlertViewBlocks.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 12/5/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "UIAlertViewBlocks.h"

@interface UIAlertViewBlocks ()

@property (nonatomic, copy) void(^clickBlock)(int);

@end

@implementation UIAlertViewBlocks


-(id)initWithTitle:(NSString*)title message:(NSString*)message acceptButtonTitle:(NSString*)buttonTitle clickBlock:(void(^)(int))clickBlock{
    
    self = [self initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:buttonTitle, nil];
    self.clickBlock = clickBlock;
    self.delegate = self;
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma -mark UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex != self.cancelButtonIndex){
        self.clickBlock((int)buttonIndex);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
