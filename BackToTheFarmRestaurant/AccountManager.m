//
//  AccountManager.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 11/25/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "AccountManager.h"
#import "AccountVerificationManager.h"
#import "FileManager.h"
#import "Constants.h"

@interface AccountManager ()

@property (nonatomic, strong) FileManager *fileManager;

@end

@implementation AccountManager

-(id)init{
    self = [super init];
    
    //init other vars
    self.fileManager = [[FileManager alloc] init];
    
    return self;
}

-(void)loginWithCompletion:(void(^)(BOOL))completion{
    [self.fileManager loadAccountFromCacheWithCompletion:^(NSDictionary *accountInfo) {
        if(accountInfo) {
            [self loginWithUsername:accountInfo[@"email"] andPassword:accountInfo[@"password"]successCompletion:^(NSDictionary *results) {
                self.user = results[@"account"];
                completion(YES);
            } failureCompletion:^(int status) {
                completion(NO);
            }];
        } else{
            completion(NO);
        }
    }];
}

-(void)loginWithUsername:(NSString*)username andPassword:(NSString*)password successCompletion:(void(^)(NSDictionary*))successCompletion failureCompletion:(void(^)(int))failureCompletion{
    
    //verify
    BOOL success = YES;//[AccountVerificationManager verifyAccountUsername:username andPassword:password];
    if(success){
        [self.fileManager loadAccountFromDatabaseWithUsername:username andPassword:password completion:^(id
                                                                                                         results) {
            NSDictionary *resultsDict = results;
            int status = ((NSNumber*)resultsDict[@"status"]).intValue;
            switch (status) {
                case LOGIN_SUCCESS:
                    self.user = resultsDict[@"account"];
                    [self successHandler:resultsDict[@"account"] completion:successCompletion];
                    break;
                default:
                    failureCompletion(status);
                    break;
            }
        }];
    } else{
        int status = USERNAME_BAD_SYNTAX;
        failureCompletion(status);
    }
    
}

-(void)placeOrder:(PFObject*)order{
        PFRelation *orderRelation = [self.user relationforKey:@"orders"];
        [orderRelation addObject:order];
        [self.user saveInBackground];
}

//do what you want upon success
-(void)successHandler:(id)object completion:(void(^)(id))completion{
    completion(object);
}

//---
-(NSDictionary*)loadCachedFoodOptions{
    return [self.fileManager loadObjectClass:[NSDictionary class] ForCacheKey:CACHE_KEY_FOODS];
}

-(void)loadFoodOptionsFromDatabaseWithCompletion:(void(^)(NSDictionary*))completion{
    [PFCloud callFunctionInBackground:@"retrieveFoods" withParameters:@{} block:^(id object, NSError *error) {
        NSArray *retrievedFoods = object[@"foods"];
        
        //if cached foods, load them, if not init dictionary
        NSMutableDictionary *cachedFoods = [self loadCachedFoodOptions].mutableCopy;
        if(!cachedFoods || !cachedFoods.count){
            cachedFoods = [NSMutableDictionary new];
        }
        
        //merged pulled foods with saved
        BOOL cacheChanged = NO;
        for (NSDictionary *food in retrievedFoods) {
            if(!cachedFoods[food[@"name"]]){
                [cachedFoods setObject:food forKey:food[@"name"]];
                cacheChanged = YES;
            }
        }
        
        //save back merged list
        if(cacheChanged){
            [self.fileManager asyncSaveObject:cachedFoods toCacheForKey:CACHE_KEY_FOODS];
        }
        
        //return list
        completion(cachedFoods.copy);
    }];
}

@end
