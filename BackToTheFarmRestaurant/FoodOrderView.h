//
//  FoodOrderView.h
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 12/3/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STPView.h"

@class PFObject;
@interface FoodOrderView : UIView <STPViewDelegate>

-(void)show;
-(id)initWithFood:(PFObject*)food andCompletion:(void(^)(BOOL, NSError*))completionBlock;

@end
