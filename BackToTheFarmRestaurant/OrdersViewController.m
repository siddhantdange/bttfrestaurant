//
//  OrdersViewController.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 12/3/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "OrdersViewController.h"

#import "AppDelegate.h"
#import "AccountManager.h"
#import <Parse/Parse.h>

@interface OrdersViewController ()

@property (nonatomic, weak) IBOutlet UITableView *ordersTableView;
@property (nonatomic, strong) AccountManager *acManager;

@end

@implementation OrdersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.ordersTableView setDataSource:self];
    [self.ordersTableView setDelegate:self];
    [self.ordersTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellView"];
    
    self.acManager = ((AccountManager*)((AppDelegate*)[UIApplication sharedApplication].delegate).accountManager);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma -mark UITableView Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)self.acManager.user[@"orders"]).count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellView"];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellView"];
    }
    
    PFObject *order = self.acManager.user[@"orders"][indexPath.row];
    [cell.textLabel setText:[NSString stringWithFormat:@"%@ : %@", order[@"food"], order[@"quantity"]]];
    
    return cell;
}

#pragma -mark UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end
