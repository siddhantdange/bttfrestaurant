//
//  UICollectionViewCell+FoodCell.h
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 12/1/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagePreview;
@property (weak, nonatomic) IBOutlet UILabel *foodNameLabel;



@end
