//
//  FileManager.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 11/25/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "FileManager.h"
#import <Parse/Parse.h>
#import "Constants.h"
//#import "PFObject+Extension.h"

@implementation FileManager

#pragma -mark Database Interaction

-(void)saveAccountToDatabaseWithUsername:(NSString*)username andPassword:(NSString*)password completion:(void(^)(BOOL))completion{
    //save account to parse
    PFObject *account = [[PFObject alloc] initWithClassName:CACHE_KEY_ACCOUNT];
    account[@"email"] = username;
    account[@"password"] = password;
    [account saveInBackground];
    
    //completion
    BOOL success = YES;
    completion(success);
}

-(void)loadAccountFromDatabaseWithUsername:(NSString*)username andPassword:(NSString*)password completion:(void(^)(id))completion{
    
    //load parse account
    [PFCloud callFunctionInBackground:@"checkAccount" withParameters:@{@"email" : username, @"password" : password} block:^(id object, NSError *error) {
        completion(object);
    }];
}

#pragma -mark Local Storage Interaction

-(BOOL)isAccountCached{
    return NO;
}

-(void)loadAccountFromCacheWithCompletion:(void(^)(NSDictionary*))completion{
    NSDictionary *accountInfo = nil;
    
    //if account is cached, load from there
    if([self isAccountCached]){
        accountInfo = [self loadObjectClass:[NSDictionary class] ForCacheKey:CACHE_KEY_ACCOUNT];
    }
    
    completion(accountInfo);
}


-(void)cacheAccount:(NSString*)username andPassword:(NSString*)password completion:(void(^)(BOOL))completion{
    
    //cache account
    [self asyncSaveObject:@{@"email" : username, @"password" : password} toCacheForKey:CACHE_KEY_ACCOUNT];
    
    //completion
    BOOL success = YES;
    completion(success);
}

//---

/*---*/

//save object to cache async
-(void)asyncSaveObject:(id)object toCacheForKey:(NSString*)cacheKey{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [NSKeyedArchiver archiveRootObject:object toFile:[self fileNameWithCacheKey:cacheKey]];
    });
}

//synchronously load data from cache key with specificed class
-(id)loadObjectClass:(Class)class ForCacheKey:(NSString*)cacheKey{
    NSData *data = [NSKeyedUnarchiver unarchiveObjectWithFile:[self fileNameWithCacheKey:cacheKey]];
    
    id unarchivedData = nil;
    
    if([class isSubclassOfClass:[NSDictionary class]]){
        unarchivedData = (NSDictionary*)data;
    } else if([class isSubclassOfClass:[NSArray class]]){
        unarchivedData = (NSArray*)data;
    }
    
    return unarchivedData;
}

-(NSString*)fileNameWithCacheKey:(NSString*)key{
    return [[self baseFilePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.plist", key]];
}

-(NSString*)baseFilePath{
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
}

@end
