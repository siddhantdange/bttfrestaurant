//
//  FoodOrderView.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 12/3/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "FoodOrderView.h"
#import "AccountManager.h"
#import "AppDelegate.h"
#import "PopupAlert.h"
#import "Constants.h"
#import "UIAlertViewBlocks.h"

#import <QuartzCore/QuartzCore.h>

#import <Parse/Parse.h>

@interface FoodOrderView ()

@property (nonatomic, strong) UILabel *orderLabel, *resultLabel;
@property (nonatomic, assign) int orderValue;
@property (nonatomic, assign) PFObject *foodObj;
@property (nonatomic, copy) void(^completionBlock)(BOOL, NSError*);
@property (nonatomic, strong) UIButton *orderButton;
@property (nonatomic, strong) STPView *stripeView;
@property (nonatomic, strong) UISwitch *saveSwitch;
@property (nonatomic, strong) UILabel *saveSwitchLabel;
@property (nonatomic, assign) BOOL creditCardMode;

@end

@implementation FoodOrderView

-(id)initWithFood:(PFObject*)food andCompletion:(void(^)(BOOL, NSError*))completionBlock{
    //assign object
    self.foodObj = food;
    self.completionBlock = completionBlock;
    self.creditCardMode = NO;
    
    self = [self initWithFrame:CGRectMake(10.0, -280.0, 300.0, 270.0)];

    //image background
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.frame];
    [backgroundImageView setImage:[UIImage new]];
    [self addSubview:backgroundImageView];
    
    //set blur
    UITabBar *tab = [[UITabBar alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.           size.height)];
    [self addSubview:tab];
    [self bringSubviewToFront:tab];
    
    //set title label
    float labelWidth = 100.0f;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - labelWidth)/2.0, 10.0, labelWidth, 50.0f)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setText:self.foodObj[@"name"]];
    [self addSubview:titleLabel];
    
    //set order label
    float orderLabelWidth = self.frame.size.width * 0.8;
    self.orderLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - orderLabelWidth)/ 2.0, self.frame.size.height/2.0 - 50.0f, orderLabelWidth, 50.0f)];
    [self.orderLabel setTextAlignment:NSTextAlignmentCenter];
    [self.orderLabel setText:[NSString stringWithFormat:@"%.2f   X   %d   =   $%0.2f", ((NSNumber*)self.foodObj[@"price"]).floatValue, 0, 0.0f]];
    [self addSubview:self.orderLabel];
    self.orderValue = 0;
    
    //addition and subraction buttons
    CGColorRef greyColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1.0].CGColor;
    float buttonWidth = 100.0f;
    float edgeMargin = 30.0f;
    
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - buttonWidth - edgeMargin, self.orderLabel.frame.origin.y + self.orderLabel.frame.size.height, buttonWidth, 40.0f)];
    [addButton setTitle:@"+" forState:UIControlStateNormal];
    [addButton.layer setBorderWidth:1.0f];
    [addButton.layer setBorderColor:greyColor];
    [addButton setTag:1];
    [addButton addTarget:self action:@selector(orderValueChanged:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:addButton];
    
    UIButton *subtractButton = [[UIButton alloc] initWithFrame:CGRectMake(edgeMargin, addButton.frame.origin.y, addButton.frame.size.width, addButton.frame.size.height)];
    [subtractButton setTitle:@"-" forState:UIControlStateNormal];
    [subtractButton.layer setBorderWidth:1.0f];
    [subtractButton.layer setBorderColor:greyColor];
    [subtractButton setTag:0];
    [subtractButton addTarget:self action:@selector(orderValueChanged:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:subtractButton];
    
    //order button
    float orderButtonWidth = self.frame.size.width;
    float orderButtonHeight = 60.0f;
    self.orderButton = [[UIButton alloc] initWithFrame:CGRectMake((self.frame.size.width - orderButtonWidth)/2.0, self.frame.size.height - orderButtonHeight, orderButtonWidth, orderButtonHeight)];
    [self.orderButton.layer setBorderWidth:1.0f];
    [self.orderButton.layer setBorderColor:greyColor];
    [self.orderButton setTitle:@"Place Order" forState:UIControlStateNormal];
    [self.orderButton addTarget:self action:@selector(placeOrder:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.orderButton];
    
    //cancel button
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10.0, 0.0, 70.0, 50.0)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(simpleCancel:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:cancelButton];
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma -mark Functionality

//place order
-(void)placeOrder:(id)sender{
    if(self.orderValue){
        [self switchModes];
    } else{
        PopupAlert *popup = [[PopupAlert alloc] initWithTime:0.2f message:@"Order is 0" completionBlock:nil];
        [popup show];
    }
}

-(void)orderValueChanged:(id)sender{
    
    //if in credit card mode, switch back
    if(self.creditCardMode){
        [self switchModes];
    }
    
    int changedValue = 0;
    switch (((UIView*)sender).tag) {
        case 0: //subtract
            changedValue = -1;
            break;
        case 1: //add
            changedValue = 1;
            break;
            
        default:
            break;
    }
    
    //if number of orders is realisitc
    if(self.orderValue + changedValue >= 0){
        self.orderValue += changedValue;
    }
    
    [self.orderLabel setText:[NSString stringWithFormat:@"%.2f   X   %d   =   $%0.2f", ((NSNumber*)self.foodObj[@"price"]).floatValue, self.orderValue, self.orderValue * ((NSNumber*)self.foodObj[@"price"]).floatValue]]; //((NSNumber*)self.foodObj[@"price"]).floatValue
}

#pragma -mark UI Changes

-(void)show{
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    [UIView animateWithDuration:0.2f animations:^{
        [self setFrame:CGRectApplyAffineTransform(self.frame, CGAffineTransformMakeTranslation(0.0, 355.0f))];
    }];
}

//switch modes between credit card mode and not
-(void)switchModes{
    if(!self.creditCardMode){
        
        //if stripeview hasnt been initialized
        if(!self.stripeView){
            self.stripeView = [[STPView alloc] initWithFrame:CGRectMake(5.0f, self.frame.size.height - 55.0f, 300.0f, 55.0f) andKey:@"pk_test_LpLUnyk4VCiH0MtvLqf2SZGu"];
            [self.stripeView setBackgroundColor:[UIColor clearColor]];
            [self.stripeView setDelegate:self];
        }
        
        if(!self.saveSwitch){
            
            //switch
            self.saveSwitch = [[UISwitch alloc] init];
            [self.saveSwitch setFrame:CGRectMake(10.0f, self.stripeView.frame.origin.y - self.saveSwitch.frame.size.height - 5.0f, self.saveSwitch.frame.size.width, self.saveSwitch.frame.size.height)];
            [self.saveSwitch setTintColor:[UIColor lightGrayColor]];
            [self.saveSwitch setOnTintColor:[UIColor lightGrayColor]];
            
            //label
            self.saveSwitchLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.saveSwitch.frame.origin.x + self.saveSwitch.frame.size.width + 10.0f, self.saveSwitch.frame.origin.y + ((self.saveSwitch.frame.size.height - 30.0f)/2.0), 100.0f, 30.0f)];
            [self.saveSwitchLabel setText:@"Remember Card?"];
        }
        
        //animation with removing order button and adding stripeview
        [UIView animateWithDuration:0.3f animations:^{
            [self.orderButton setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [self.orderButton removeFromSuperview];
            [self.stripeView setAlpha:0.0f];
            [self.saveSwitch setAlpha:0.0f];
            [self.saveSwitchLabel setAlpha:0.0f];
            [self addSubview:self.stripeView];
            [self addSubview:self.saveSwitch];
            [self addSubview:self.saveSwitchLabel];
            
            [UIView animateWithDuration:0.2f animations:^{
                [self.stripeView setAlpha:1.0f];
                [self.saveSwitch setAlpha:1.0f];
                [self.saveSwitchLabel setAlpha:1.0f];
            }];
        }];
    } else{
        
        //animation with removing stripeview adding order button
        [UIView animateWithDuration:0.3f animations:^{
            [self.stripeView setAlpha:0.0f];
            [self.saveSwitch setAlpha:0.0f];
            [self.saveSwitchLabel setAlpha:0.0f];
            
        } completion:^(BOOL finished) {
            [self.stripeView removeFromSuperview];
            [self.saveSwitch removeFromSuperview];
            [self.saveSwitchLabel removeFromSuperview];
            [self.orderButton setAlpha:0.0f];
            [self addSubview:self.orderButton];
            
            [UIView animateWithDuration:0.2f animations:^{
                [self.orderButton setAlpha:1.0f];
            }];
        }];
    }
    
    self.creditCardMode = !self.creditCardMode;
}

-(void)simpleCancel:(id)sender{
    [self cancelSelf:sender withSuccess:NO andError:nil];
}

//fade self away
-(void)cancelSelf:(id)sender withSuccess:(BOOL)success andError:(NSError*)error{
    [UIView animateWithDuration:0.2f animations:^{
        [self setFrame:CGRectApplyAffineTransform(self.frame, CGAffineTransformMakeTranslation(0.0, -380.0f))];
    } completion:^(BOOL finished) {
        if(finished){
            [self removeFromSuperview];
            self.completionBlock(success, error);
        }
    }];
}

#pragma -mark STPView Delegate

-(void)stripeView:(STPView *)view withCard:(PKCard *)card isValid:(BOOL)valid{
    if(valid) {
        UIAlertViewBlocks *alertView = [[UIAlertViewBlocks alloc] initWithTitle:@"Order?" message:@"Are you sure? Order cannot be reversed" acceptButtonTitle:@"Yes" clickBlock:^(int buttonIndex) {
            if(buttonIndex){
                [self.stripeView createToken:^(STPToken *token, NSError *error) {
                    if(!error){
                        
                        //save order to database if user has checkboxed
                        AccountManager *acManager = ((AppDelegate*)[UIApplication sharedApplication].delegate).accountManager;
                        
                        //if user specified to remember credit card
                        if(self.saveSwitch.isOn){
                            acManager.user[@"stripeToken"] = token.tokenId;
                        }
                        

                        PFObject *order = [[PFObject alloc] initWithClassName:@"Order"];
                        //food, quantity, total cost
                        order[@"food"] = self.foodObj[@"name"];
                        order[@"quantity"] = @(self.orderValue);
                        order[@"totalCost"] = @(self.orderValue * ((NSNumber*)self.foodObj[@"price"]).floatValue);

                        [order saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if(succeeded){
                             //   [acManager placeOrder:order];
                                
                                [PFCloud callFunctionInBackground:@"chargeRestaurant" withParameters:@{@"email" : acManager.user[@"email"], @"totalCost" : @(((NSNumber*)order[@"totalCost"]).intValue * 100), @"stripeToken" : token.tokenId} block:^(id object, NSError *error) {
                                    NSLog(@"result: %@, error: %@, token: %@, totalCost: %@, message: %@", object, error, token.tokenId, order[@"totalCost"], object[@"responseMessage"]);
                                    
                                    int status = ((NSNumber*)object[@"status"]).intValue;
                                    
                                    PopupAlert *popup = [[PopupAlert alloc] initWithTime:0.3f message:@"Ordered!" completionBlock:nil];
                                    [popup show];
                                    
                                    BOOL worked = (error != nil);
                                    [self cancelSelf:self withSuccess:worked andError:error];
                                }];
                            }
                        }];
                        
                    } else{
                        PopupAlert *popup = [[PopupAlert alloc] initWithTime:0.3f message:@"Card Analysis Error" completionBlock:nil];
                        [popup show];
                    }
                }];
            }
        }];
        [alertView show];
    } else{
        PopupAlert *popup = [[PopupAlert alloc] initWithTime:0.3f message:@"Invalid Card" completionBlock:nil];
        [popup show];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
