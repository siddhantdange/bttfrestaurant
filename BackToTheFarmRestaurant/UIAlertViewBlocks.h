//
//  UIAlertViewBlocks.h
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 12/5/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertViewBlocks : UIAlertView <UIAlertViewDelegate>

-(id)initWithTitle:(NSString*)title message:(NSString*)message acceptButtonTitle:(NSString*)buttonTitle clickBlock:(void(^)(int))clickBlock;

@end
