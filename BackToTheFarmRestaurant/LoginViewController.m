//
//  LoginViewController.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 11/25/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "AccountManager.h"

@interface LoginViewController ()
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UISwitch *rememberSwitch;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)loginAction:(id)sender {
    NSString *username = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;
    
    AccountManager *acManager = ((AppDelegate*)[UIApplication sharedApplication].delegate).accountManager;
    [acManager loginWithUsername:username andPassword:password successCompletion:^(NSDictionary *dataPassback) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            PFRelation *relation = [acManager.user relationforKey:@"orders"];
            [relation.query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(!error && objects){
                    acManager.user[@"orders"] = objects;
                }
            }];
        });
        
        [self dismissViewControllerAnimated:YES completion:nil];
    } failureCompletion:^(int status) {
        NSLog(@"status: %d", status);
    }];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
