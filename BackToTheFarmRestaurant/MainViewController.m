//
//  ViewController.m
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 11/25/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "AccountManager.h"

#import "FoodCell.h"
#import "FoodOrderView.h"
#import "PopupAlert.h"

@interface MainViewController ()

@property (nonatomic, weak) IBOutlet UICollectionView *foodCollectionView;
@property (nonatomic, strong) NSDictionary *foodOptions;
@property (nonatomic, strong) NSMutableArray *foodArrayOptions;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
  //  [self.foodCollectionView registerClass:[FoodCell class] forCellWithReuseIdentifier:@"FoodCell"];
    UINib *nib = [UINib nibWithNibName:@"FoodCell" bundle:nil];
    [self.foodCollectionView registerNib:nib forCellWithReuseIdentifier:@"FoodCell"];
    
    [self.foodCollectionView setDataSource:self];
    [self.foodCollectionView setDelegate:self];
    self.foodArrayOptions = [NSMutableArray new];
    
    //check login
    AccountManager *acManager = ((AppDelegate*)[UIApplication sharedApplication].delegate).accountManager;
    [acManager loginWithCompletion:^(BOOL success) {
        if(!success){
            [self performSegueWithIdentifier:@"loginSegue" sender:self];
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    AccountManager *acManager = ((AppDelegate*)[UIApplication sharedApplication].delegate).accountManager;
    self.foodOptions = [acManager loadCachedFoodOptions];
    [acManager loadFoodOptionsFromDatabaseWithCompletion:^(NSDictionary *loadedFoodOptions) {
        self.foodOptions = loadedFoodOptions;
        for(NSString *key in loadedFoodOptions.allKeys){
            [self.foodArrayOptions addObject:loadedFoodOptions[key]];
        }
        
        [self.foodCollectionView reloadData];
    }];
    
    NSLog(@"view appeared");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark UICollectionView Data Source

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.foodOptions.count;

}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FoodCell *cell = (FoodCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"FoodCell" forIndexPath:indexPath];
    
    PFObject *food = self.foodArrayOptions[indexPath.row];
    [cell.foodNameLabel setText:food[@"name"]];
    
    return cell;
}

#pragma -mark UICollectionViewFlowLayoutDelegate


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(130, 130.0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PFObject *food = self.foodArrayOptions[indexPath.row];
    FoodOrderView *orderView = [[FoodOrderView alloc] initWithFood:food andCompletion:^(BOOL success, NSError *error) {
        if(!error){ //if operation was successful, depends on whether transaction happened or not
            if(success){
                PopupAlert *alert = [[PopupAlert alloc] initWithTime:0.5 message:@"Ordered!" completionBlock:nil];
                [alert show];
            }
        } else{ //if there's an error
            NSString *message = error.description;
            PopupAlert *alert = [[PopupAlert alloc] initWithTime:0.5 message:@"Error Ordering" completionBlock:nil];
            [alert show];
        }
    }];
    [orderView show];
}

@end
