//
//  AccountManager.h
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 11/25/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface AccountManager : NSObject


@property (nonatomic, strong) PFObject *user;
@property (nonatomic, strong) NSDictionary *foods;

-(void)loginWithCompletion:(void(^)(BOOL))completion;
-(void)loginWithUsername:(NSString*)username andPassword:(NSString*)password successCompletion:(void(^)(NSDictionary*))successCompletion failureCompletion:(void(^)(int))failureCompletion;

-(NSDictionary*)loadCachedFoodOptions;
-(void)loadFoodOptionsFromDatabaseWithCompletion:(void(^)(NSDictionary*))completion;

-(void)placeOrder:(PFObject*)order;


@end
