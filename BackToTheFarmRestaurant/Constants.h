//
//  Constants.h
//  BackToTheFarmRestaurant
//
//  Created by Siddhant Dange on 11/26/13.
//  Copyright (c) 2013 Siddhant Dange. All rights reserved.
//

#ifndef BackToTheFarmRestaurant_Constants_h
#define BackToTheFarmRestaurant_Constants_h

#pragma -mark Account Verification Status

#define USERNAME_BAD_SYNTAX -0x00000001;

#pragma -mark Login Status

#define LOGIN_SUCCESS 1
#define LOGIN_WRONG_PASSWORD -2
#define LOGIN_NO_ACCOUNT -1
#define LOGIN_ERROR -1000

#pragma -mark Caching

#define CACHE_KEY_FOODS @"Food"
#define CACHE_KEY_ACCOUNT @"Restaurant"
#define CACHE_KEY_ORDERS @"Order"

#pragma -mark Object Attributes

#define OBJECT_ACCOUNT_ATTRIBUTES @[@"email", @"password"]
#define OBJECT_FOODS_ATTRIBUTES @[@"name", @"serving", @"price"]

#endif
